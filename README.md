## Python OpenWeather application
Simple Python program to fetch OpenWeather data.

## Table of contents
* [Technologies](#technologies)
* [Setup](#setup)

## Technologies
Project is created with:
* Python3
    
## Setup
To run this project, it's required to install these libraries:
```
python3 -m venv .venv
source .venv/bin/activate
apt install libgirepository1.0-dev
pip3 install -r requirements.txt
python3 ./OpenWeather.py
```
