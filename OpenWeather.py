#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Program:      Python OpenWeather applications
# Author:       Elvis Baketa
# Version:      0.3
#               Python3
#               PyGObject (pip install PyGObject)
#               requests (pip install requests)
#               jsons (pip install jsons)

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk

import time
import requests, json
from dotenv import dotenv_values

class MyWindow(Gtk.Window):
    def __init__(self): 
        Gtk.Window.__init__(self)
        self.set_title("OpenWeather")
        self.connect("delete-event", Gtk.main_quit)
        self.connect("key-press-event",self.on_key_press_event)
        self.connect("realize", self.on_realize)
        self.set_name('MyWindow')
        self.set_resizable(True)
        # set window position
        self.set_position(Gtk.WindowPosition.CENTER)

        # get current time and date
        self.currentTime=time.strftime("%H:%M", time.localtime())
        self.currentDate=time.strftime("%d. %b %Y", time.localtime())

        # last update time
        self.lastUpdateTime=""
        self.lastUpdateDate=""

        # fullscreen toggling variable
        self.fullscreen_toggler = True
        # hide mouse pointer toggling variable
        self.mousePointer_toggler = False
        # city names variable
        self.city_name= ["Gutach im Breisgau", "Lovas", "Waldkirch", "Vukovar", "Freiburg im Breisgau", "Bocholt", "Hechingen"]
        self.cityToDisplayCount = 0
        self.cityToDisplay = self.city_name[self.cityToDisplayCount]

        # wrapper for holding all elements of a window
        wrapper = Gtk.Box()
        wrapper.set_name('wrapper')
        wrapper.set_size_request(800,480)
        wrapper.set_orientation(Gtk.Orientation.VERTICAL)
        wrapper.set_homogeneous(False)
        wrapper.set_margin_top(5)
        wrapper.set_margin_bottom(5)
        wrapper.set_margin_start(5)
        wrapper.set_margin_end(5)
        self.add(wrapper)

        # temp
        self.lblTemp = Gtk.Label()
        self.lblTemp.set_name('lblTemp')
        self.lblTemp.set_halign(0)
        self.lblTemp.set_valign(2)
        self.lblTemp.set_margin_start(5)
        self.lblTemp.set_margin_end(5)
        self.lblTemp.set_text("00.00°C")
        wrapper.pack_start(self.lblTemp, True, True, 0)

        # image
        self.weatherImage = Gtk.Image()
        self.weatherImage.set_halign(0)
        self.weatherImage.set_valign(2)
        # self.weatherImage.set_from_file('./icons/rain.png')
        wrapper.pack_start(self.weatherImage, True, True, 0)

        # weather description
        self.lblWeatherDescription = Gtk.Label()
        self.lblWeatherDescription.set_name('lblWeatherDescription')
        self.lblWeatherDescription.set_halign(0)
        self.lblWeatherDescription.set_valign(2)
        self.lblWeatherDescription.set_margin_start(5)
        self.lblWeatherDescription.set_margin_end(5)
        self.lblWeatherDescription.set_text("weather description")
        wrapper.pack_start(self.lblWeatherDescription, True, True, 0)

        # wrapper for holding humidity and pressure
        self.wrapperHP = Gtk.Box()
        self.wrapperHP.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.wrapperHP.set_margin_start(5)
        self.wrapperHP.set_margin_end(5)
        wrapper.pack_start(self.wrapperHP, True, True, 0)

        # humidity
        self.lblHumidity = Gtk.Label()
        self.lblHumidity.set_name('lblHumidity')
        self.lblHumidity.set_halign(2)
        self.lblHumidity.set_valign(1)
        # self.lblHumidity.set_margin_start(5)
        self.lblHumidity.set_margin_end(5)
        self.lblHumidity.set_text("Humidity")
        self.wrapperHP.pack_start(self.lblHumidity, True, True, 0)

        # pressure
        self.lblPressure = Gtk.Label()
        self.lblPressure.set_name('lblPressure')
        self.lblPressure.set_halign(1)
        self.lblPressure.set_valign(1)
        self.lblPressure.set_margin_start(5)
        # self.lblPressure.set_margin_end(5)
        self.lblPressure.set_text("Pressure")
        self.wrapperHP.pack_start(self.lblPressure, True, True, 0)

        # city
        self.lblCity = Gtk.Label()
        self.lblCity.set_name('lblCity')
        self.lblCity.set_halign(0)
        self.lblCity.set_valign(1)
        self.lblCity.set_margin_start(5)
        self.lblCity.set_margin_end(5)
        self.lblCity.set_text("City name")
        wrapper.pack_start(self.lblCity, True, True, 0)

        # last update
        self.lblLastUpdate = Gtk.Label()
        self.lblLastUpdate.set_halign(0)
        self.lblLastUpdate.set_valign(1)
        self.lblLastUpdate.set_margin_start(5)
        self.lblLastUpdate.set_margin_end(5)
        wrapper.pack_start(self.lblLastUpdate, True, True, 0)

    def on_realize(self, widget, data=None):
        self.fullscreen_toggler = True
        self.mousePointer_toggler = True
        self.refreshData()
        self.lastUpdate()
        # self.lblDate.set_text(self.currentDate)
        self.hideShowMousePointer()

    # function to hide or show mouse pointer
    def hideShowMousePointer(self):
        # show mouse pointer
        if self.mousePointer_toggler == True:
            display = self.get_display()
            cursor = Gdk.Cursor.new_for_display(display, Gdk.CursorType.BLANK_CURSOR)
            self.get_window().set_cursor(cursor)
        # hide mouse pointer
        elif self.mousePointer_toggler == False:
            display = self.get_display()
            cursor = Gdk.Cursor.new_for_display(display, Gdk.CursorType.TOP_LEFT_ARROW)
            self.get_window().set_cursor(cursor)
        else:
            pass

    # function to resolve key press events
    def on_key_press_event(self, widget, event):
        if event.keyval == Gdk.KEY_F11:
            self.fullscreen_toggler = not self.fullscreen_toggler
            self.updateDisplay()
        elif event.keyval == Gdk.KEY_F1:
            helpWindow().show_all()
        elif event.keyval == Gdk.KEY_F2:
            self.cityToDisplayCount += 1
            if self.cityToDisplayCount > (len(self.city_name) - 1):
                self.cityToDisplayCount = 0
            self.cityToDisplay = self.city_name[self.cityToDisplayCount]
            self.fetchWeatherData()
            self.updateDisplay()
            self.lastUpdate()
        elif event.keyval == Gdk.KEY_F5:
            self.refreshData()
            self.lastUpdate()
        else:
            pass

    # function to update display
    def updateDisplay(self):
        if self.fullscreen_toggler == True:
            self.fullscreen()
            self.mousePointer_toggler = True
            self.hideShowMousePointer()
        elif self.fullscreen_toggler == False:
            self.unfullscreen()
            self.mousePointer_toggler = False
            self.hideShowMousePointer()
        else:
            pass

    # function to fetch OpenWeather data
    # api.openweathermap.org/data/2.5/weather?q={city name}&appid={your api key}
    # api.openweathermap.org/data/2.5/weather?q={city name},{state code}&appid={your api key}
    # api.openweathermap.org/data/2.5/weather?q={city name},{state code},{country code}&appid={your api key}
    # api.openweathermap.org/data/2.5/weather?q=Lovas&appid=a85c616abe8fb79365f1f7a02385ab09
    # api.openweathermap.org/data/2.5/weather?q=Gutach%20im%20Breisgau&appid=a85c616abe8fb79365f1f7a02385ab09
    def fetchWeatherData(self):
        secrets = dotenv_values(".env")
        base_url= secrets["API_BASEURL"]
        api_key = secrets["API_KEY"]
        city_name = self.cityToDisplay

        # create a complete URL to request data from OpenWeather
        #complete_url = base_url + "appid=" + api_key + "&q=" + city_name
        complete_url = base_url + city_name + "&appid=" + api_key

        # variable to hold requested data
        response = requests.get(complete_url)

        # fetch and parse JSON-format data
        x = response.json()

        # extract a description of the weather forecast
        y = x["weather"][0]
        weatherNow = y["description"]
        self.lblWeatherDescription.set_text(weatherNow)
        weatherIcon = y["icon"]
        # get icon and save to icon folder
        icon_complete_url="http://openweathermap.org/img/wn/" + weatherIcon + "@2x.png"
        icon_request = requests.get(icon_complete_url)
        icon_file = open("./icons/" + weatherIcon + ".png", "wb")
        icon_file.write(icon_request.content)
        icon_file.close()
        self.weatherImage.set_from_file("./icons/" + weatherIcon + ".png")

        # extract temperature from weather forecast
        y = x["main"]
        current_temperature = float(y["temp"]) - float(273.15)
        # format string to two decimal places
        current_temperature = str("%.1f" % round(current_temperature, 1))
        self.lblTemp.set_text(current_temperature + "°C")

        # extract air pressure from the weather forecast
        pressureNow = int(y["pressure"])
        self.lblPressure.set_text(str(pressureNow) + "hPa")

        # extract air humidity from the weather forecast
        humidityNow = int(y["humidity"])
        self.lblHumidity.set_text(str(humidityNow) + "%")

        # extract city name
        currentCity= x["name"]
        y = x["sys"]
        currentCountry = y["country"]
        self.lblCity.set_text(str(currentCity) + ", " + str(currentCountry))

    # function to fetch time
    def fetchTime(self):
        self.currentTime=time.strftime("%H:%M", time.localtime())

    # function to fetch date
    def fetchDate(self):
        self.currentDate=time.strftime("%d. %B", time.localtime())
        self.currentYear=time.strftime("%Y", time.localtime())

    # function to refresh data
    def refreshData(self):
        self.fetchWeatherData()
        self.updateDisplay()

    # lastUpdate
    def lastUpdate(self):
        self.lastUpdateTime=time.strftime("%H:%M", time.localtime())
        self.lastUpdateDate=time.strftime("%d. %B", time.localtime())
        self.lblLastUpdate.set_text("Last update: " + self.lastUpdateDate + ", " + self.lastUpdateTime)

class helpWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_title("Help - OpenWeather")
        # self.connect("delete-event", Gtk.main_quit)
        # set window position
        self.set_position(Gtk.WindowPosition.CENTER)
        # set default window size
        self.set_default_size(320, 200)

        # wrapper for holding all elements of a window
        wrapper = Gtk.Box()
        wrapper.set_orientation(Gtk.Orientation.VERTICAL)
        wrapper.set_margin_top(5)
        wrapper.set_margin_bottom(5)
        wrapper.set_margin_start(5)
        wrapper.set_margin_end(5)
        self.add(wrapper)

        # F1
        self.lblF1Desc = Gtk.Label()
        self.lblF1Desc.set_halign(1)
        self.lblF1Desc.set_margin_start(5)
        self.lblF1Desc.set_margin_end(5)
        self.lblF1Desc.set_text("F1\t\tShows this window")
        wrapper.pack_start(self.lblF1Desc, True, True, 0)

        # F2
        self.lblF2Desc = Gtk.Label()
        self.lblF2Desc.set_halign(1)
        self.lblF2Desc.set_margin_start(5)
        self.lblF2Desc.set_margin_end(5)
        self.lblF2Desc.set_text("F2\t\tChange the city for which data is displayed")
        wrapper.pack_start(self.lblF2Desc, True, True, 0)

        # F5
        self.lblF5Desc = Gtk.Label()
        self.lblF5Desc.set_halign(1)
        self.lblF5Desc.set_margin_start(5)
        self.lblF5Desc.set_margin_end(5)
        self.lblF5Desc.set_text("F5\t\tRefresh data")
        wrapper.pack_start(self.lblF5Desc, True, True, 0)

        # F11
        self.lblF11Desc = Gtk.Label()
        self.lblF11Desc.set_halign(1)
        self.lblF11Desc.set_margin_start(5)
        self.lblF11Desc.set_margin_end(5)
        self.lblF11Desc.set_text("F11\tSwitch to full screen mode or window mode")
        wrapper.pack_start(self.lblF11Desc, True, True, 0)

    def gtk_style():
        style_provider = Gtk.CssProvider()
        style_provider.load_from_path('theme.css')

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    gtk_style()

if __name__ == "__main__":
    win = MyWindow()
    win.show_all()
    Gtk.main()
